package Challange3;

import static org.junit.jupiter.api.Assertions.*;

import java.text.DecimalFormat;
import java.util.ArrayList;

import org.junit.jupiter.api.Test;

class TestChallange3 {
	@org.junit.jupiter.api.Test
	void TestMean() {
		ArrayList<Integer>DitData = Challange3.getData();
		final DecimalFormat df = new DecimalFormat("0.00");
		assertEquals("8.32", df.format(Challange3.mean(DitData)));
	}
	@org.junit.jupiter.api.Test
	void TestMedian() {
		ArrayList<Integer>DitData = Challange3.getData();

		assertEquals(8.0, Challange3.median(DitData));
	}
	@org.junit.jupiter.api.Test
	void TestModus() {
		ArrayList<Integer>DitData = Challange3.getData();

		assertEquals(7.0, Challange3.modus(DitData));
	}
}
