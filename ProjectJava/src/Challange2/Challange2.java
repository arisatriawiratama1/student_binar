package Challange2;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Scanner;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;


public class Challange2 {
	
	public static ArrayList<Integer>getData(){
		String data = "C:\\temp\\direktori\\data_sekolah.csv";
		ArrayList<Integer>DataList = new ArrayList();
		
		try {
			File file = new File(data);
			FileReader fr = new FileReader(file);
			BufferedReader br = new BufferedReader(fr);
			String line = "";
			String[] tempArr;
			
			while((line = br.readLine()) != null) {
				tempArr = line.split(";");
				for(String s : tempArr) {
					if (s.equals(tempArr[0]));
				else {
					DataList.add(Integer.valueOf(s));
				}
				}
			}
			br.close();
		} catch (IOException e) {
			// TODO: handle exception
			System.out.println("----------------------------------");
			System.out.println("Aplikasi Pengolah Nilai Siswa");
			System.out.println("----------------------------------");
			System.out.println("File tidak ditemukan");
			System.out.println(" ");
			System.out.println("Harap Periksa File Anda");
			System.out.println("Letakan file csv dengan nama file data_sekolah di direktori berikut : C://temp/direktori ");
			System.out.println(" ");
			System.exit(0);
		}
		return DataList;
		
	}
	public static float mean(ArrayList<Integer>dataAngka) {
		float mean = 0;
		int size = dataAngka.size();
		for (float j : dataAngka) {
			mean = mean + j;
		}
		float hasil = mean / size;
		return hasil;
		
	}
	public static float median(ArrayList<Integer>dataNilai) {
		float median = 0;
		int bk = dataNilai.size();
		Collections.sort(dataNilai);
		if (bk%2==0) {
			median = dataNilai.get(bk/2)+dataNilai.get(bk/2-1);
			median = median/2;
		}else
		{
			median = dataNilai.get(bk/2);
		}
		return median;
		
	}
	public static float modus(ArrayList<Integer>modus) {
		Integer[]DataInt = null;
		DataInt = new Integer [modus.size()];
		for (int i = 0; i<modus.size();i++) {
			DataInt [i] = modus.get(i);
		}
		int nm = 0;
		int cm = 0;
		
		int[] counts = new int[modus.size()];
		for (int i = 0; i < DataInt.length; i++) {
			counts[DataInt[i]]++;
			if (cm<counts[DataInt[i]]) {
				cm = counts[DataInt[i]];
				nm = DataInt[i];
			}
		}
		return nm;
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ArrayList<Integer>DitData = getData();
		while (true) {
			int menu;
			Scanner scan = new Scanner (System.in);
			System.out.println("----------------------------------");
			System.out.println("Aplikasi Pengolah Nilai Siswa");
			System.out.println("----------------------------------");
			System.out.println("Letakan file csv dengan nama file data_sekolah di direktori berikut : C://temp/direktori ");
			System.out.println(" ");
			System.out.println("Pilih menu: ");
			System.out.println("1. Generate txt untuk menampilkan modus");
			System.out.println("2. Generate txt untuk menampilkan nilai rata-rata,median");
			System.out.println("3. Generate kedua file");
			System.out.println("0. Exit");
			System.out.println(" ");
			System.out.print("Pilihan anda : ");
			menu = scan.nextInt();
			
			switch (menu) {
			case 1:
			
			String valueofmodus = String.valueOf(modus(DitData));
			try {
				FileWriter fr = new FileWriter("C:\\temp\\direktori\\DataModus.txt");
				BufferedWriter br = new BufferedWriter(fr);
				br.write("Hasil dari modus adalah");
				br.newLine();
				br.write(valueofmodus);
				System.out.println(valueofmodus);
				br.flush();
				br.close();
				break;
				
			} catch (IOException e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			
			
			case 2:
				String valueofmedian = String.valueOf(median(DitData));
				String valueofmean = String.valueOf(mean(DitData));
				try {
					FileWriter fr = new FileWriter("C:\\temp\\direktori\\DataMedianMean.txt");
					BufferedWriter br = new BufferedWriter(fr);
					br.write("Hasil median adalah");
					br.newLine();
					br.write(valueofmedian);
					br.newLine();
					br.write("Hasil dari mean adalah");
					br.newLine();
					br.write(valueofmean);
					System.out.println(mean(DitData));
					System.out.println(median(DitData));
					br.flush();
					br.close();
					break;
				} catch (IOException e) {
					// TODO: handle exception
					e.printStackTrace();
				}
				
				
			case 3:
			
				String datamodus = String.valueOf(modus(DitData));
				String datamedian = String.valueOf(median(DitData));
				String datamean = String.valueOf(mean(DitData));
				
				try {
					FileWriter fr = new FileWriter("C:\\temp\\direktori\\DataModusMedianMean.txt");
					BufferedWriter br = new BufferedWriter(fr);
					br.write("Hasil dari modus adalah");
					br.newLine();
					br.write(datamodus);
					br.newLine();
					br.write("Hasil dari median adalah");
					br.newLine();
					br.write(datamedian);
					br.newLine();
					br.write("Hasil dari mean adalah");
					br.newLine();
					br.write(datamean);
					System.out.println(modus(DitData));
					System.out.println(mean(DitData));
					System.out.println(median(DitData));
					br.flush();
					br.close();
					
				} catch (IOException e) {
					// TODO: handle exception
					e.printStackTrace();
				}
				
				break;

			default :
				System.out.println("Anda Telah Exit");
				break;
				
			}
			
		}
		
		
	}

	}



